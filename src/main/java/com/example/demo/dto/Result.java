package com.example.demo.dto;

public class Result {

	private Object message;
	private String remark;
	private boolean success;
	public Object getMessage() {
		return message;
	}
	public void setMessage(Object message) {
		this.message = message;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Result(Object message, String remark, boolean success) {
		super();
		this.message = message;
		this.remark = remark;
		this.success = success;
	}
	public Result() {
		super();
	}
	
}
