package com.example.demo.entity;

import java.sql.Timestamp;

/**
 * 员工签到类
 */
public class UserCheck {
	private int id; //签到Id 
	private String userId;//用户Id
	private String userName;//用户名称
	private String checkState;//签到状态
	private String isCancel;//是否取消签到：未取消，取消
	private Timestamp checkInTime;//签到时间
	private Timestamp checkOutTime;//取消签到时间
	
	public UserCheck() {
	}
	
	public UserCheck(String userId, String userName, String checkState) {
		this.userId = userId;
		this.userName = userName;
		this.checkState = checkState;
	}

	public UserCheck(String userId, String checkState, String isCancel, Timestamp checkOutTime) {
		this.userId = userId;
		this.checkState = checkState;
		this.isCancel = isCancel;
		this.checkOutTime = checkOutTime;
	}

	public UserCheck(int id, String userId, String userName, String checkState, String isCancel, Timestamp checkInTime,
			Timestamp checkOutTime) {
		this.id = id;
		this.userId = userId;
		this.userName = userName;
		this.checkState = checkState;
		this.isCancel = isCancel;
		this.checkInTime = checkInTime;
		this.checkOutTime = checkOutTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(Timestamp checkInTime) {
		this.checkInTime = checkInTime;
	}

	public Timestamp getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(Timestamp checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCheckState() {
		return checkState;
	}

	public void setCheckState(String checkState) {
		this.checkState = checkState;
	}

	public String getIsCancel() {
		return isCancel;
	}

	public void setIsCancel(String isCancel) {
		this.isCancel = isCancel;
	}	
}
