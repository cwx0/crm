package com.example.demo.entity;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "user")

public class User {
	@Id //实体类的主键

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer uid;
	@Column(length = 50,nullable = false)	
	private String loginName; // 登录名
	@Column(length = 50,nullable = false)
	private String password; // 密码
	@Column(columnDefinition = "char(2) default '否'",insertable = false)
	private String isLockout; // 是否锁定
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Column(columnDefinition = "datetime default now()",insertable = false,updatable = false)
	private Date createTime; // 账号创立时间
	private Date lastLoginTime; // 最后一次登录时间
	private Date lockTime; // 被锁定时间
	@Column(columnDefinition = "int default 0",insertable = false)
	private Integer psdWrongTime; // 密码错误次数
	private String protectEmail; // 密保邮箱
	private String protectMTel; // 密保手机号
	private String loginState;//登陆状态
	

	
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public Set<Role> getRoleSet() {
		return roleSet;
	}
	public void setRoleSet(Set<Role> roleSet) {
		this.roleSet = roleSet;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIsLockout() {
		return isLockout;
	}
	public void setIsLockout(String isLockout) {
		this.isLockout = isLockout;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public Date getLockTime() {
		return lockTime;
	}
	public void setLockTime(Date lockTime) {
		this.lockTime = lockTime;
	}
	public Integer getPsdWrongTime() {
		return psdWrongTime;
	}
	public void setPsdWrongTime(Integer psdWrongTime) {
		this.psdWrongTime = psdWrongTime;
	}
	public String getProtectEmail() {
		return protectEmail;
	}
	public void setProtectEmail(String protectEmail) {
		this.protectEmail = protectEmail;
	}
	public String getProtectMTel() {
		return protectMTel;
	}
	public void setProtectMTel(String protectMTel) {
		this.protectMTel = protectMTel;
	}
	public String getLoginState() {
		return loginState;
	}
	public void setLoginState(String loginState) {
		this.loginState = loginState;
	}
	
	@Override
	public String toString() {
		return "User [uid=" + uid + ", loginName=" + loginName + ", password=" + password + ", isLockout=" + isLockout
				+ ", createTime=" + createTime + ", lastLoginTime=" + lastLoginTime + ", lockTime=" + lockTime
				+ ", psdWrongTime=" + psdWrongTime + ", protectEmail=" + protectEmail + ", protectMTel=" + protectMTel
				+ ", loginState=" + loginState + ", roleSet=" + roleSet + "]";
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User( int uid, String loginName, String password, String isLockout, Date createTime,
			Date lastLoginTime, Date lockTime, Integer psdWrongTime, String protectEmail, String protectMTel,
			String loginState) {
		super();
	
		this.uid = uid;
		this.loginName = loginName;
		this.password = password;
		this.isLockout = isLockout;
		this.createTime = createTime;
		this.lastLoginTime = lastLoginTime;
		this.lockTime = lockTime;
		this.psdWrongTime = psdWrongTime;
		this.protectEmail = protectEmail;
		this.protectMTel = protectMTel;
		this.loginState = loginState;
	}
	
	
	@ManyToMany
	@Cascade(CascadeType.SAVE_UPDATE)
	@JoinTable(name = "userrole",joinColumns = @JoinColumn(name="userid"),inverseJoinColumns = @JoinColumn(name="roleid"))
	@NotFound(action = NotFoundAction.IGNORE)
	private Set<Role> roleSet = new HashSet<Role>();


}
