package com.example.demo.entity;

import java.sql.Timestamp;

public class Asker {
	private String askerId;// 咨询师Id
	private String askerName;// 咨询师名称
	private String checkState;// 签到状态
	private Timestamp checkInTime;// 签到时间
	private String changeState;// 是否分配学生
	private int weight;// 权重
	private String roleName;//角色名称
	private String bakContent;//备注内容

	public Asker() {//构造方法

	}

	public Asker(String askerId, String askerName) {
		this.askerId = askerId;
		this.askerName = askerName;
	}

	public Asker(String askerId, String checkState, Timestamp checkInTime) {
		this.askerId = askerId;
		this.checkState = checkState;
		this.checkInTime = checkInTime;
	}
	public Asker(String askerId, int weight, String bakContent) {
		this.askerId = askerId;
		this.weight = weight;
		this.bakContent = bakContent;
	}

	public String getAskerId() {
		return askerId;
	}

	public void setAskerId(String askerId) {
		this.askerId = askerId;
	}

	public String getAskerName() {
		return askerName;
	}

	public void setAskerName(String askerName) {
		this.askerName = askerName;
	}

	public String getCheckState() {
		return checkState;
	}

	public void setCheckState(String checkState) {
		this.checkState = checkState;
	}

	public Timestamp getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(Timestamp checkInTime) {
		this.checkInTime = checkInTime;
	}

	public String getChangeState() {
		return changeState;
	}

	public void setChangeState(String changeState) {
		this.changeState = changeState;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getBakContent() {
		return bakContent;
	}

	public void setBakContent(String bakContent) {
		this.bakContent = bakContent;
	}

	@Override
	public String toString() {
		return "Asker [askerId=" + askerId + ", askerName=" + askerName + ", checkState=" + checkState
				+ ", checkInTime=" + checkInTime + ", changeState=" + changeState + ", weight=" + weight + ", roleName="
				+ roleName + ", bakContent=" + bakContent + "]";
	}

}
