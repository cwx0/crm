package com.example.demo.entity;

import java.sql.Timestamp;
/**
 * 学生类
 */
public class Student {
	/** ---------------网络咨询人员录入相关字段------------------------------ **/
	private int id; // 编号
	private String name; // 学员姓名
	private String sex; // 性别 0:男，1：女
	private int age;// 年龄
	private String phone; // 学员电话
	private String stuStatus; // 学历
	private String perState; // 个人状态
	private String msgSource; // 来源渠道
	private String sourceUrl; // 来源网址
	private String sourceKeyWord;// 来源关键词
	private String address; // 所在区域
	private String netPusherId;// 网络咨询Id
	private String askerId;// 咨询师Id
	private String createUser;// 录入人姓名
	private String isBaoBei;// 是否报备0：未报备，1：已报备
	private String stuConcern;// 学员关注
	private String qq;// 学员QQ
	private String weiXin;// 微信
	private String content;// 备注
	/** ---------------网络咨询人员录入相关字段------------------------------ **/

	/** ---------------咨询师录入相关字段------------------------------ **/
	private String learnForward;// 课程方向
	private String isValid;// 是否有效 0：无效，1：有效
	private String record;// 打分
	private String isReturnVist; // 是否回访 0：不回访，1：回访
	private Timestamp firstVisitTime;// 首次回访时间
	private String isHome;// 是否上门 0：未上门，1：上门
	private Timestamp homeTime;// 上门时间
	private String lostValid;// 无效原因
	private String isPay;// 是否付款 0：未付款，1：已付款
	private Timestamp payTime; // 付款时间
	private double money; // 金额
	private String isReturnMoney;// 是否退费 0：未退费，1：已退费
	private String isInClass;// 是否进班 0：未进班，1：已进班
	private Timestamp inClassTime;// 进班时间
	private String inClassContent;// 进班备注
	private String askerContent; // 咨询师备注
	private String isDel;// 是否删除0：未删除，1：已删除
	private String fromPart;// 来源部门
	private String ziXunName;// 咨询师填写的姓名
	private String returnMoneyReason;// 退费原因
	private double preMoney;// 预付订金
	private Timestamp preMoneyTime;// 预付定金时间
	private Timestamp createTime;// 创建时间

	/** ---------------咨询师录入相关字段------------------------------ **/
	public Student() {

	}

	public Student(String name, String sex, int age, String phone, String stuStatus, String perState, String msgSource,
			String sourceUrl, String sourceKeyWord, String address, String netPusherId, String askerId, String createUser,
			String isBaoBei, String stuConcern, String qq, String weiXin, String content) {
		this.name = name;
		this.sex = sex;
		this.age = age;
		this.phone = phone;
		this.stuStatus = stuStatus;
		this.perState = perState;
		this.msgSource = msgSource;
		this.sourceUrl = sourceUrl;
		this.sourceKeyWord = sourceKeyWord;
		this.address = address;
		this.netPusherId = netPusherId;
		this.askerId = askerId;
		this.createUser = createUser;
		this.isBaoBei = isBaoBei;
		this.stuConcern = stuConcern;
		this.qq = qq;
		this.weiXin = weiXin;
		this.content = content;
	}

	public Student(String learnForward, String isValid, String record, String isReturnVist, Timestamp firstVisitTime,
			String isHome, Timestamp homeTime, String lostValid, String isPay, Timestamp payTime, double money,
			String isReturnMoney, String isInClass, Timestamp inClassTime, String inClassContent, String askerContent,
			String isDel, String fromPart, String ziXunName, String returnMoneyReason, double preMoney,
			Timestamp preMoneyTime) {
		this.learnForward = learnForward;
		this.isValid = isValid;
		this.record = record;
		this.isReturnVist = isReturnVist;
		this.firstVisitTime = firstVisitTime;
		this.isHome = isHome;
		this.homeTime = homeTime;
		this.lostValid = lostValid;
		this.isPay = isPay;
		this.payTime = payTime;
		this.money = money;
		this.isReturnMoney = isReturnMoney;
		this.isInClass = isInClass;
		this.inClassTime = inClassTime;
		this.inClassContent = inClassContent;
		this.askerContent = askerContent;
		this.isDel = isDel;
		this.fromPart = fromPart;
		this.ziXunName = ziXunName;
		this.returnMoneyReason = returnMoneyReason;
		this.preMoney = preMoney;
		this.preMoneyTime = preMoneyTime;
	}
 
	public Student(String name, String sex, int age, String phone, String stuStatus, String perState, String msgSource,
			String sourceUrl, String sourceKeyWord, String address, String netPusherId, String askerId,
			String createUser, String isBaoBei, String stuConcern, String qq, String weiXin, String content,
			String learnForward, String isValid, String record, String isReturnVist, Timestamp firstVisitTime,
			String isHome, Timestamp homeTime, String lostValid, String isPay, Timestamp payTime, double money,
			String isReturnMoney, String isInClass, Timestamp inClassTime, String inClassContent, String askerContent,
			String isDel, String fromPart, String ziXunName, String returnMoneyReason, double preMoney,
			Timestamp preMoneyTime, Timestamp createTime) {
		this.name = name;
		this.sex = sex;
		this.age = age;
		this.phone = phone;
		this.stuStatus = stuStatus;
		this.perState = perState;
		this.msgSource = msgSource;
		this.sourceUrl = sourceUrl;
		this.sourceKeyWord = sourceKeyWord;
		this.address = address;
		this.netPusherId = netPusherId;
		this.askerId = askerId;
		this.createUser = createUser;
		this.isBaoBei = isBaoBei;
		this.stuConcern = stuConcern;
		this.qq = qq;
		this.weiXin = weiXin;
		this.content = content;
		this.learnForward = learnForward;
		this.isValid = isValid;
		this.record = record;
		this.isReturnVist = isReturnVist;
		this.firstVisitTime = firstVisitTime;
		this.isHome = isHome;
		this.homeTime = homeTime;
		this.lostValid = lostValid;
		this.isPay = isPay;
		this.payTime = payTime;
		this.money = money;
		this.isReturnMoney = isReturnMoney;
		this.isInClass = isInClass;
		this.inClassTime = inClassTime;
		this.inClassContent = inClassContent;
		this.askerContent = askerContent;
		this.isDel = isDel;
		this.fromPart = fromPart;
		this.ziXunName = ziXunName;
		this.returnMoneyReason = returnMoneyReason;
		this.preMoney = preMoney;
		this.preMoneyTime = preMoneyTime;
		this.createTime = createTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStuStatus() {
		return stuStatus;
	}

	public void setStuStatus(String stuStatus) {
		this.stuStatus = stuStatus;
	}

	public String getPerState() {
		return perState;
	}

	public void setPerState(String perState) {
		this.perState = perState;
	}

	public String getMsgSource() {
		return msgSource;
	}

	public void setMsgSource(String msgSource) {
		this.msgSource = msgSource;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getSourceKeyWord() {
		return sourceKeyWord;
	}

	public void setSourceKeyWord(String sourceKeyWord) {
		this.sourceKeyWord = sourceKeyWord;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNetPusherId() {
		return netPusherId;
	}

	public void setNetPusherId(String netPusherId) {
		this.netPusherId = netPusherId;
	}

	public String getAskerId() {
		return askerId;
	}

	public void setAskerId(String askerId) {
		this.askerId = askerId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getIsBaoBei() {
		return isBaoBei;
	}

	public void setIsBaoBei(String isBaoBei) {
		this.isBaoBei = isBaoBei;
	}

	public String getStuConcern() {
		return stuConcern;
	}

	public void setStuConcern(String stuConcern) {
		this.stuConcern = stuConcern;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWeiXin() {
		return weiXin;
	}

	public void setWeiXin(String weiXin) {
		this.weiXin = weiXin;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLearnForward() {
		return learnForward;
	}

	public void setLearnForward(String learnForward) {
		this.learnForward = learnForward;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public String getIsReturnVist() {
		return isReturnVist;
	}

	public void setIsReturnVist(String isReturnVist) {
		this.isReturnVist = isReturnVist;
	}

	public Timestamp getFirstVisitTime() {
		return firstVisitTime;
	}

	public void setFirstVisitTime(Timestamp firstVisitTime) {
		this.firstVisitTime = firstVisitTime;
	}

	public String getIsHome() {
		return isHome;
	}

	public void setIsHome(String isHome) {
		this.isHome = isHome;
	}

	public Timestamp getHomeTime() {
		return homeTime;
	}

	public void setHomeTime(Timestamp homeTime) {
		this.homeTime = homeTime;
	}

	public String getLostValid() {
		return lostValid;
	}

	public void setLostValid(String lostValid) {
		this.lostValid = lostValid;
	}

	public String getIsPay() {
		return isPay;
	}

	public void setIsPay(String isPay) {
		this.isPay = isPay;
	}

	public Timestamp getPayTime() {
		return payTime;
	}

	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getIsReturnMoney() {
		return isReturnMoney;
	}

	public void setIsReturnMoney(String isReturnMoney) {
		this.isReturnMoney = isReturnMoney;
	}

	public String getIsInClass() {
		return isInClass;
	}

	public void setIsInClass(String isInClass) {
		this.isInClass = isInClass;
	}

	public Timestamp getInClassTime() {
		return inClassTime;
	}

	public void setInClassTime(Timestamp inClassTime) {
		this.inClassTime = inClassTime;
	}

	public String getInClassContent() {
		return inClassContent;
	}

	public void setInClassContent(String inClassContent) {
		this.inClassContent = inClassContent;
	}

	public String getAskerContent() {
		return askerContent;
	}

	public void setAskerContent(String askerContent) {
		this.askerContent = askerContent;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getFromPart() {
		return fromPart;
	}

	public void setFromPart(String fromPart) {
		this.fromPart = fromPart;
	}

	public String getZiXunName() {
		return ziXunName;
	}

	public void setZiXunName(String ziXunName) {
		this.ziXunName = ziXunName;
	}

	public String getReturnMoneyReason() {
		return returnMoneyReason;
	}

	public void setReturnMoneyReason(String returnMoneyReason) {
		this.returnMoneyReason = returnMoneyReason;
	}

	public double getPreMoney() {
		return preMoney;
	}

	public void setPreMoney(double preMoney) {
		this.preMoney = preMoney;
	}

	public Timestamp getPreMoneyTime() {
		return preMoneyTime;
	}

	public void setPreMoneyTime(Timestamp preMoneyTime) {
		this.preMoneyTime = preMoneyTime;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", sex=" + sex + ", age=" + age + ", phone=" + phone
				+ ", stuStatus=" + stuStatus + ", perState=" + perState + ", msgSource=" + msgSource + ", sourceUrl="
				+ sourceUrl + ", sourceKeyWord=" + sourceKeyWord + ", address=" + address + ", netPusherId="
				+ netPusherId + ", askerId=" + askerId + ", createUser=" + createUser + ", isBaoBei=" + isBaoBei
				+ ", stuConcern=" + stuConcern + ", qq=" + qq + ", weiXin=" + weiXin + ", content=" + content
				+ ", learnForward=" + learnForward + ", isValid=" + isValid + ", record=" + record + ", isReturnVist="
				+ isReturnVist + ", firstVisitTime=" + firstVisitTime + ", isHome=" + isHome + ", homeTime=" + homeTime
				+ ", lostValid=" + lostValid + ", isPay=" + isPay + ", payTime=" + payTime + ", money=" + money
				+ ", isReturnMoney=" + isReturnMoney + ", isInClass=" + isInClass + ", inClassTime=" + inClassTime
				+ ", inClassContent=" + inClassContent + ", askerContent=" + askerContent + ", isDel=" + isDel
				+ ", fromPart=" + fromPart + ", ziXunName=" + ziXunName + ", returnMoneyReason=" + returnMoneyReason
				+ ", preMoney=" + preMoney + ", preMoneyTime=" + preMoneyTime + ", createTime=" + createTime + "]";
	}
}
