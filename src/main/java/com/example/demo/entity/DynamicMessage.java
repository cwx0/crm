package com.example.demo.entity;

import java.sql.Timestamp;

/**
 * 动态消息表
 */
public class DynamicMessage {
	private int id;// 消息Id
	private int studentId;// 学生Id
	private String studentName;//学生名称
	private String messageContent;// 消息内容
	private String isOpen;// 消息是否打开 0：未打开，1：已打开
	private Timestamp createTime;// 消息创建时间

	public DynamicMessage() {

	}

	public DynamicMessage(int studentId, String studentName, String messageContent, String isOpen,
			Timestamp createTime) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.messageContent = messageContent;
		this.isOpen = isOpen;
		this.createTime = createTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String isOpen() {
		return isOpen;
	}

	public void setOpen(String isOpen) {
		this.isOpen = isOpen;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(String isOpen) {
		this.isOpen = isOpen;
	}
}
