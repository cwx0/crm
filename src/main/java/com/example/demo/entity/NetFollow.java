package com.example.demo.entity;

import java.sql.Timestamp;

/**
 * 网络跟踪类
 */
public class NetFollow {
	private int id; // 跟踪Id
	private int studentId;// 学生Id
	private String studentName;//学生姓名
	private String userId;// 用户Id
	private String content;// 备注
	private String followType;// 跟踪类型
	private Timestamp followTime;// 跟踪时间
	private Timestamp nextFollowTime;// 下次跟踪时间
	private Timestamp createTime;// 创建时间
	private String followState;// 跟踪状态

	public NetFollow() {
	}

	public NetFollow(int studentId, String studentName, String userId, String content, String followType,
			Timestamp followTime, Timestamp nextFollowTime, String followState) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.userId = userId;
		this.content = content;
		this.followType = followType;
		this.followTime = followTime;
		this.nextFollowTime = nextFollowTime;
		this.followState = followState;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFollowType() {
		return followType;
	}

	public void setFollowType(String followType) {
		this.followType = followType;
	}

	public Timestamp getFollowTime() {
		return followTime;
	}

	public void setFollowTime(Timestamp followTime) {
		this.followTime = followTime;
	}

	public Timestamp getNextFollowTime() {
		return nextFollowTime;
	}

	public void setNextFollowTime(Timestamp nextFollowTime) {
		this.nextFollowTime = nextFollowTime;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getFollowState() {
		return followState;
	}

	public void setFollowState(String followState) {
		this.followState = followState;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
}
