package com.example.demo.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.JoinColumn;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="roles")
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private int rid; // 角色编号UUID
	@Column(length = 50,nullable = false)
	private String name; // 角色	
	private Integer int0; // 预留Int
	@Column(length = 50,nullable = false)
	private String string0; // 预留字符串


	
	
	@Override
	public String toString() {
		return "Role [rid=" + rid + ", name=" + name + ", int0=" + int0 + ", string0=" + string0 + ", userSet="
				+ userSet + "]";
	}




	public int getRid() {
		return rid;
	}




	public void setRid(int rid) {
		this.rid = rid;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public Integer getInt0() {
		return int0;
	}




	public void setInt0(Integer int0) {
		this.int0 = int0;
	}




	public String getString0() {
		return string0;
	}




	public void setString0(String string0) {
		this.string0 = string0;
	}




	public Set<User> getUserSet() {
		return userSet;
	}




	public void setUserSet(Set<User> userSet) {
		this.userSet = userSet;
	}




	@JsonIgnore
	@ManyToMany
	@Cascade(CascadeType.SAVE_UPDATE)
	@JoinTable(name="userrole",joinColumns = @JoinColumn(name="roleid"),inverseJoinColumns = @JoinColumn(name="userid"))
	@NotFound(action = NotFoundAction.IGNORE)//找不到外键引用时忽略
	private Set<User> userSet = new HashSet<User>();
	/*
	@ManyToMany
	@Cascade(CascadeType.SAVE_UPDATE)
	@JoinTable(name="userrole",joinColumns = @JoinColumn(name="roleid"),inverseJoinColumns = @JoinColumn(name="moduleid"))
	@NotFound(action = NotFoundAction.IGNORE)//找不到外键引用时忽略
	private Set<Module> moduleSet = new HashSet<Module>();
*/

}
