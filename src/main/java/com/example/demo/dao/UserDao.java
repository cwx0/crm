package com.example.demo.dao;





import javax.transaction.Transactional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


import com.example.demo.entity.User;

public interface UserDao extends JpaRepository<User, Integer>,JpaSpecificationExecutor<User>{
	//登录
	User findByLoginNameAndPassword(String loginName,String password) ;
	
	//修改
	@Modifying(clearAutomatically=true)
	@Transactional
	@Query("update User u set u.password = ?1 where u.uid = ?2")
	int updatepwd(String npwd,Integer uid);
	//根据id查询pwd	
	@Query("select password from User where uid =:uid")
	String findPwdById(Integer uid);
	
	
	
	
}
