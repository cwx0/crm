package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Module;

public interface ModulesDao extends JpaRepository<Module, Integer>{

}
