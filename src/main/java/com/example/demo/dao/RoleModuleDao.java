package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Module;

public interface RoleModuleDao extends JpaRepository<Module, Integer>{

}
