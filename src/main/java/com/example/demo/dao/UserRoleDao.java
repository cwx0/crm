package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.UserRole;

public interface UserRoleDao extends JpaRepository<UserRole, Integer>{

	@Query("select id from UserRole ur where ur.uid =:uid and ur.rid = :rid")
	int findByUidAndRid(int uid,int rid);
}

