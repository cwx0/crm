package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.ModulesDao;
import com.example.demo.entity.Module;



@RestController
@RequestMapping("/modules")
public class ModulesController {
	
	@Autowired
	private ModulesDao md;
	
	@RequestMapping("/findAll")
	public Object findAll() {
		List<Module> modules = md.findAll();
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("code", 0);
		map.put("msg", "操作成功");
		map.put("data", modules);
		//System.out.println(user);
		return map;
	}
	@RequestMapping("save")
	public void save(Module module) {
		md.save(module);
	}
	
	@RequestMapping("delOne")
	public void delOne(int id) {
		md.deleteById(id);
	}


}
