package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.RoleDao;
import com.example.demo.dto.RoleDTO;
import com.example.demo.entity.Role;

@RestController
@RequestMapping("/role")
public class RoleController {

	@Resource
	private RoleDao repository;

	@RequestMapping("/findAll")
	public Map<String, Object> findAll() {
		List<Role> list = repository.findAll();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("count", 100);
		map.put("data", list);
		return map;
	}

	/*
	 * public Map<String, Object> findAll(RoleDTO dto) {
	 * 
	 * 
	 * // 准备查询条件 Specification<Role> specification = new Specification<Role>() {
	 * 
	 * @Override public Predicate toPredicate(Root<Role> root, CriteriaQuery<?>
	 * query, CriteriaBuilder criteriaBuilder) { Predicate predicate =
	 * criteriaBuilder.conjunction();// 动态SQL表达式 List<Expression<Boolean>> exList =
	 * predicate.getExpressions();// 表达式集合 if (dto.getName() != null &&
	 * !"".equals(dto.getName())) {// 用户名模糊查询
	 * exList.add(criteriaBuilder.like(root.<String>get("name"), "%" + dto.getName()
	 * + "%")); } System.out.println(predicate); return predicate;
	 * 
	 * } };
	 * 
	 * // 准备分页条件 Pageable pageable = new PageRequest(dto.getPage(), dto.getLimit());
	 * Page<Role> page = repository.findAll(specification, pageable); Map<String,
	 * Object> map = new HashMap<String, Object>(); map.put("code", 0);
	 * map.put("count", page.getSize()); map.put("data", page.getContent());
	 * System.out.println(page); return map;
	 * 
	 * }
	 */

	@RequestMapping("/delRole")
	public void delRole(Role id) {
		repository.delete(id);

	}

	@RequestMapping("save")
	public void save(Role role) {
		repository.save(role);
	}

}
