package com.example.demo.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.UserDao;
import com.example.demo.dao.UserRoleDao;
import com.example.demo.dto.Result;
import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;
import com.example.demo.entity.UserRole;

@RestController
@RequestMapping("/users")
public class UserController {
	@Autowired
	private UserDao service;
	@Resource
	private UserRoleDao userRoleDao;
	private Iterable<Integer> ids;

	@RequestMapping("/findAll")
	public Map findAll(UserDto dto) {
		// 准备查询条件
		Specification<User> s = new Specification<User>() {
			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				// 动态sql表达式
				Predicate p = criteriaBuilder.conjunction();
				// 表达式集合
				List<Expression<Boolean>> exList = p.getExpressions();
				if (dto.getLoginName() != null && !"".equals(dto.getLoginName())) {
					// 用户名模糊查询
					exList.add(criteriaBuilder.like(root.<String>get("loginName"), "%" + dto.getLoginName() + "%"));
				}
				if (dto.getStart() != null) {// 大于或等于起始日期
					exList.add(criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("createTime"), dto.getStart()));
				}
				if (dto.getEnd() != null) {// 小于或等于结束日期
					exList.add(criteriaBuilder.lessThanOrEqualTo(root.<Date>get("createTime"), dto.getEnd()));
				}
				return p;
			}
		};

		// 准备分页条件
		Pageable pageable = new PageRequest((dto.getPage() - 1) * dto.getLimit(), dto.getLimit());
		Page<User> page = service.findAll(s, pageable);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("count", page.getSize());
		map.put("data", page.getContent());
		return map;
	}

//登陆
	@RequestMapping("/login")
	public Object login(User user) {
		return service.findByLoginNameAndPassword(user.getLoginName(), user.getPassword());
	}

	// 修改密码
	@RequestMapping("/updatepwd")
	public Object updatepwd(HttpServletRequest request) {
		Map<String, String> map=new HashMap<String, String>();		
		String npwd=request.getParameter("newpass");
		String upwd=request.getParameter("oldpass");
		String uid=request.getParameter("uid");	
		String repass=request.getParameter("repass");
		System.out.println(npwd+":"+upwd+":"+uid);
		String pwd=service.findPwdById(Integer.valueOf(uid));
		Result rst=new Result();
		if(npwd.equals(repass)) {
		if(pwd.equals(upwd)) {
			int k=service.updatepwd( npwd,Integer.valueOf(uid));
			if(k>0) {
				
				rst.setRemark("修改成功");
				rst.setSuccess(true);
			
			}else {
				rst.setRemark("修改失败，出现异常");
				rst.setSuccess(false);				
			}
		
		}else {
			rst.setRemark("原密码输入错误");
			rst.setSuccess(false);
				
		}
		}else{
			rst.setRemark("请保证新密码与确认密码相同");
			rst.setSuccess(false);
				
		}
			
		return rst;
		
	}

	// id查询
	@RequestMapping("findById")
	public Object findById(Integer id){
		return service.findById(id);
	}

	// 添加或更新
	@RequestMapping("save")
	public void save(User user) {
		System.out.println(user.toString());
		service.save(user);
	}

	// id删除
	@RequestMapping("delOne")
	public void delOne(User id) {
		service.delete(id);

	}

	// 分配角色
	@RequestMapping("/setRole")
	public void setRole(UserRole ur, String checked) {
		if (checked.equals("true")) {
			userRoleDao.save(ur);
		} else {
			int id = userRoleDao.findByUidAndRid(ur.getUid(), ur.getRid());
			userRoleDao.deleteById(id);
		}

	}

}
